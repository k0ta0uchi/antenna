<?php
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

require '../vendor/autoload.php';

// Set up settings
$settings = require __DIR__ . '/../src/settings.php';

define("ARTICLE_SIZE", 50);

// Init Slim app
$app = new \Slim\App($settings);

// Set up dependencies
require __DIR__ . '/../src/dependencies.php';

// Init database connection
$app->getContainer()->get('db');

// Top page
$app->get('/[/{p}]', function (Request $request, Response $response, $argv) {
    return $this->view->render($response, "index.html");
});

// Blog
$app->get('/blog[/{id}/[{p}]]', function (Request $request, Response $response, $argv) {
    return $this->view->render($response, "index.html");
});

// Montly Ranking
$app->get('/monthly', function (Request $request, Response $response, $argv) {
    return $this->view->render($response, "index.html");
});

// Regsiter RSS page
$app->get('/registerRss', function (Request $request, Response $response) {
    return $this->view->render($response, "registerRss.html");
});

// About page
$app->get('/about', function (Request $request, Response $response, $argv) {
    return $this->view->render($response, "about.html");
});

// Updates page
$app->get('/updates', function (Request $request, Response $response, $argv) {
    return $this->view->render($response, "updates.html");
});

/*-------------------------------------------------------------------------
  APIs
------------------------------------------------------------------------*/

// Registration page
$app->post('/api/registerRss', function(Request $request, Response $response) {
    $url = $request->getParam('url');
    try {
        $rssfeed = Feed::loadRss($url);
        if (!count($rssfeed->item)) {
            $rssfeed = simplexml_load_file ( $url );
            if ($rssfeed === false) {
                throw new FeedException();
            }
        }
    } catch (FeedException $e) {
        $rssfeed = Feed::loadAtom($url);
    } catch (FeedException $e) {
        $this->logger->addError("failed loading feed: " . $url);
        return $response->withStatus(500);
    }

    // 重複確認
    if (!(Rss::where('rss', $url)->count())) {
        $rssrow = new Rss();
        $rssrow->rss = $url;
        if ($rssfeed->channel->link) {
            $rssrow->url = $rssfeed->channel->link;
            $rssrow->title = $rssfeed->channel->title;
        } else if ($rssfeed->link != "") {
            $rssrow->url = $rssfeed->link;
            $rssrow->title = $rssfeed->title;
        } else {
           foreach($rssfeed->link->attributes()['href'] as $a => $b) {
               $rssrow->url = $b;
           }
           $rssrow->title = $rssfeed->title;
       }
       if ($rssrow->url == "") {
           $rssrow->url = $rssfeed->link[0]['href'];
       }
        $rssrow->save();
        $this->logger->addInfo($rssfeed->title . "が追加されました。");
        return RSS::all()->toJson();
    }
    return $response->withStatus(500);
});

// List RSSes
$app->get('/api/listRsses', function(Request $request, Response $response) {
    return Rss::all()->toJson();
});

// Monthly Ranking
$app->get('/api/monthlyRanking', function(Request $request, Response $response) {
    return Article::whereMonth('article_date', '=', date('m'))
      ->orderBy('views', 'desc')
      ->orderBy('article_date', 'desc')
      ->limit(30)->get()->toJson();
});

// Delete Target RSS
$app->delete('/api/deleteRss', function(Request $request, Response $response) {
    $id = $request->getParam('id');
    $delrow = Rss::find($id);
    $delrow->delete();
    return;
});

// List RSSes
$app->post('/api/listArticles', function(Request $request, Response $response) {
    $page = $request->getParam("page");
    $checkboxes = $request->getParam("checkboxes");

    $deleteBlogs = [];
    foreach ($checkboxes as $checkbox) {
      if ($checkbox['checked'] == "false") {
        array_push($deleteBlogs, $checkbox['title']);
      }
    }
    $min = -(ARTICLE_SIZE) + $page * ARTICLE_SIZE;
    if(count($deleteBlogs) > 0) {
        $wherequery = "";
        for ($i = 1; $i <= count($deleteBlogs); $i++) {
            $wherequery = $wherequery . 'blogtitle != ? and ';
        }

        $wherequery = substr($wherequery, 0, $wherequery.length - 5);
        return Article::whereRaw($wherequery, $deleteBlogs)
          ->orderBy('article_date', 'desc')
          ->limit(ARTICLE_SIZE)
          ->offset($min)
          ->get()
          ->toJson();
    } else {
        return Article::orderBy('article_date', 'desc')
          ->limit(ARTICLE_SIZE)
          ->offset($min)
          ->get()
          ->toJson();
    }
});

$app->post('/api/getPageNum', function(Request $request, Response $response) {
    $checkboxes = $request->getParam("checkboxes");
    $deleteBlogs = [];
    foreach ($checkboxes as $checkbox) {
      if ($checkbox['checked'] == "false") {
        array_push($deleteBlogs, $checkbox['title']);
      }
    }
    if(count($deleteBlogs) > 0) {
        $wherequery = "";
        for ($i = 1; $i <= count($deleteBlogs); $i++) {
            $wherequery = $wherequery . 'blogtitle != ?';
            if ($i != count($deleteBlogs)) {
                $wherequery = $wherequery . ' and ';
            }
        }
        return json_encode(array('num' => ceil(Article::whereRaw($wherequery, $deleteBlogs)->count() / ARTICLE_SIZE)));
    } else {
        return json_encode(array('num' => ceil(Article::all()->count() / ARTICLE_SIZE)));
    }
});

$app->post('/api/addView', function(Request $request, Response $response) {
    $url = $request->getParam('url');
    $article = Article::where('url', $url)->first();
    $article->views = $article->views + 1;
    $article->save();
    return json_encode(array('views' => $article->views));
});

$app->run();
