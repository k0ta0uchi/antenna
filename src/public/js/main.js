const PAGE_WIDTH = 640;
const PAGE_LIMIT = 10;
const MOBILE_PAGE_LIMIT = 6;
const TITLE_WIDTH = 600;
const POLL_INTERVAL = 1000;
const SWIPE_SPEED = 2;
const SWIPE_DEADZONE = 20;
const ANIMATION_DURATION = 150;
const ANIMATION_DELAY = 50;
const MOVE_ANIMATION_DURATION = 5;
const MENU_SCROLL_THREDHOLD = 120;
const MENU_START_SCROLL_THREDHOLD = 40;
const RSS_LOAD_DELAY = 500;
const DIVISOR = 10;
const MORE_LOAD_THREDHOLD = 0.9;
const SCROLL_FREEZE_THREDHOLD = -10;
const FPS = 30;

var isAnimated = false;
var isSwiping = false;
var sX;
var sY;
var frameTime = 1000 / FPS;
var timeoutID;
var scrollTimeoutID = null ;	// タイマーの管理
var scrolling = false ;		// スクロールのステータス (true=スクロール中、false=静止中)
var isMenu = false;
var headerHeight;
var directionUp = true;
var menuTop;
var menuBottom;

window.addEventListener('scroll', function(e) {
	scrolling = true ;
	if( scrollTimeoutID ) {
		clearTimeout( scrollTimeoutID ) ;
	}
	scrollTimeoutID = setTimeout( function() {
		scrolling = false ;
		scrollTimeoutID = null ;
	}, 500 ) ;
}) ;

// global touch events
window.addEventListener('touchstart', function(e) {
  // e.preventDefault();
	var pos = getPosition(e);
	sX = pos.x;
  sY = pos.y;
},{passive: false});

// suppress vertical scrolling when swiping menues and other
window.addEventListener('touchmove', function(e) {
	var pos = getPosition(e);
  directionUp = true;
	if (Math.abs(pos.x-sX) > SWIPE_DEADZONE) isSwiping = true;
  if (pos.y-sY > 0) directionUp = false;
	if(isSwiping)
    // (isMenu && menuTop >= $(window).scrollTop() - headerHeight && !directionUp) ||
    // (isMenu && menuBottom <= $(window).scrollTop()+$(window).height()-headerHeight && directionUp))
     e.preventDefault();
  sY = getPosition(e).y;
},{passive: false});

window.addEventListener('touchend', function(e) {
	isSwiping = false;
	return true;
},{passive: true});

// Rendering App function
function renderApp(){
  ReactDOM.render (
    <AntennaApp pollInterval={POLL_INTERVAL}/>,
    document.getElementById('content')
  );
};

// Deep copy object;
function copy(o) {
	var out, v, key;
	out = Array.isArray(o) ? [] : {};
	for (key in o) {
		v = o[key];
		out[key] = (typeof v === "object") ? copy(v) : v;
	}
	return out;
};

// Return current page number
function getPage() {
  var regexp = /\?p=([0-9]*)$/;
  var page = 1;
  if (window.location.href.match(regexp)) {
    page = (window.location.href.match(regexp))[1];
  }
  return page;
};

// Return blogid or undefined;
function getBlogId () {
	var regexp = /blog\/([0-9]*)\//;
	if (window.location.href.match(regexp)){
		return window.location.href.match(regexp)[1];
	}
	return undefined;
};

function isRanking () {
  var regexp = /monthly/;
  return window.location.href.match(regexp) ? true : false;
}

// Return device variation
function getDevice() {
	var u = window.navigator.userAgent.toLowerCase();
  var mobile =
    (u.indexOf("windows") != -1 && u.indexOf("phone") != -1)
    || u.indexOf("iphone") != -1
    || u.indexOf("ipod") != -1
    || (u.indexOf("android") != -1 && u.indexOf("mobile") != -1)
    || (u.indexOf("firefox") != -1 && u.indexOf("mobile") != -1)
    || u.indexOf("blackberry") != -1;
  var tablet =
		(u.indexOf("windows") != -1 && u.indexOf("touch") != -1)
    || u.indexOf("ipad") != -1
    || (u.indexOf("android") != -1 && u.indexOf("mobile") == -1)
    || (u.indexOf("firefox") != -1 && u.indexOf("tablet") != -1)
    || u.indexOf("kindle") != -1
    || u.indexOf("silk") != -1
    || u.indexOf("playbook") != -1;
  if (mobile) {
		return "sp";
	} else if (tablet) {
		return "tablet";
	} else {
		return "pc";
	}
}

// get Touch position
function getPosition(e){
  var x   = e.touches[0].pageX;
  var y   = e.touches[0].pageY;
  x = Math.floor(x);
  y = Math.floor(y);
  var pos = {'x':x , 'y':y};
  return pos;
}

function shadow(ele, isOn) {
  if (isOn) {
    $(ele).addClass('shadow');
  } else {
    $(ele).removeClass('shadow');
  }
}

// Google AdSense Component
var Ads = React.createClass({
  componentDidMount: function() {
     (window.adsbygoogle = window.adsbygoogle || []).push({});
  },
  render: function() {
    var style = {
      display: "block",
      width: this.props.width ? this.props.width : null,
      height: this.props.height ? this.props.height : null
    };
    return (
			<div className="AdSpace">
      <ins
        className="adsbygoogle"
        style={style}
        data-ad-client={DATA_AD_CLIENT}
        data-ad-slot={DATA_AD_SLOT}
        data-ad-format="auto"
      />
			</div>
    )
  }
});

// RSS List
var RssList = React.createClass({
	componentDidMount: function() {
		var blogid = getBlogId();
		if (blogid) {
			setTimeout(() => {
				var ele = this.refs["rss" + blogid];
				ele.className = ele.className + " selected";
			}, RSS_LOAD_DELAY);
		} else if (isRanking()) {
      var className = this.refs.Monthly.className;
      this.refs.Monthly.className = className + " selected";
		} else {
			var className = this.refs.RssAll.className;
			this.refs.RssAll.className = className + " selected";
		}
		window.addEventListener('popstate', this.handleBackPage, {passive: true});
    // window.addEventListener('scroll', this.handleScroll, {passive: false});
	},
	componentWillUnmount: function() {
		window.removeEventListener('popstate', this.handleBackPage);
    // window.removeEventListener('scroll', this.handleScroll);
	},
	handleBackPage: function (){
		var rss = {
			id: getBlogId()
		};
		this.changeSelected(rss.id ? rss : isRanking() ? "monthly" : undefined, true);
	},
  onTouchEnd: function(e) {
    window.clearTimeout(timeoutID);
    if (this.marginLeft <= -PAGE_WIDTH + MENU_SCROLL_THREDHOLD) {
      isMenu = false;
      $(this.menuBox).animate({left: 0}, ANIMATION_DURATION);
      window.setTimeout(() => {
        this.articleBox.style.zIndex = "1";
        this.menuBox.style.zIndex = "-1";
        shadow(this.menuBox, false);
      }, ANIMATION_DURATION);
    } else {
      if (this.move > 0) {
        isMenu = false;
        $(this.menuBox).animate({left: 0}, ANIMATION_DURATION);
        window.setTimeout(() => {
          this.articleBox.style.zIndex = "1";
          this.menuBox.style.zIndex = "-1";
          shadow(this.menuBox, false);
        }, ANIMATION_DURATION);
      } else {
        $(this.menuBox).animate({left: PAGE_WIDTH}, ANIMATION_DURATION);
        window.setTimeout(() => {
          this.menuBox.style.zIndex = "1";
        }, ANIMATION_DURATION);
      }
    }
  },
  onTouchMove: function(e) {
    if (!isAnimated && !scrolling) {
      var x = e.nativeEvent.touches[0].clientX;
      var diffX = this.sX - x;
      var div = this.marginLeft > -PAGE_WIDTH + MENU_START_SCROLL_THREDHOLD ? 1 : DIVISOR;
      if (this.sX - x < 0) {
        this.marginLeft = this.marginLeft + Math.abs(SWIPE_SPEED*diffX/div);
      } else {
        this.marginLeft = this.marginLeft - Math.abs(SWIPE_SPEED*diffX/div);
      }
      if (this.marginLeft >= -PAGE_WIDTH && this.marginLeft <= 0 - MENU_START_SCROLL_THREDHOLD) {
        // $(this.menuBox).animate({left: this.marginLeft + PAGE_WIDTH}, {duration: MOVE_ANIMATION_DURATION, queue: false});
        this.menuBox.style.left = this.marginLeft + PAGE_WIDTH + "px";
      }
      this.move = this.sX - x;
      this.sX = x;
      isAnimated = true;
      timeoutID = window.setTimeout(function(){isAnimated=false;}, frameTime);
    }
  },
  onTouchStart: function(e) {
    this.$window = $(window);
    this.sX = e.nativeEvent.touches[0].clientX;
    this.marginLeft = this.menuBox.getBoundingClientRect().left;
    // must have to init this move onTouchStart;
    this.move = 0;
    isAnimated = true;
    timeoutID = window.setTimeout(function(){isAnimated=false;}, frameTime);
  },
  handleScroll: function(e) {
    // if (isMenu) {
    //   var scrollTop = $(window).scrollTop();
    //   if (menuTop > scrollTop - headerHeight) {
    //     $(window).scrollTop(menuTop + headerHeight);
    //     this.menuBox.style.marginTop = $(window).scrollTop() - headerHeight + "px";
    //   } else if (menuBottom + headerHeight < scrollTop + $(window).height()) {
    //     $(window).scrollTop(menuBottom - $(window).height() + headerHeight);
    //     this.menuBox.style.marginTop = Number(menuTop + (scrollTop + $(window).height() - menuBottom) - headerHeight) + "px";
    //   }
    // }
  },
	onChangeCheckbox: function(rss) {
		this.changeSelected();
		this.props.onChangeCheckbox(rss);
	},
	changeSelected: function(rss, isBack) {
		var refs = this.refs;
		Object.keys(refs).forEach(function(key) {
			var regexp = /(.*)? selected(.*)$/;
			if (refs[key].className.match(regexp)) {
				var tmp = refs[key].className.match(regexp);
				refs[key].className = tmp[1] + tmp[2];
			}
		});
		if (rss && !rss.type && rss != "monthly") {
			var ele = refs["rss" + rss.id];
			ele.className = ele.className + " selected";
			if (!isBack) this._handleRss(rss);
		} else if (rss == "monthly") {
      var ele = refs["Monthly"];
			ele.className = ele.className + " selected";
			if (!isBack) this._handleRss(rss);
		} else {
			var ele = refs["RssAll"];
			ele.className = ele.className + " selected";
			if (!isBack) this._handleRss();
		}
	},
	handleRss: function(rss) {
		this.changeSelected(rss);
		if (getDevice() == "sp") {
			$(this.menuBox).animate({left: 0}, ANIMATION_DURATION);
			window.setTimeout(() => {
				this.articleBox.style.zIndex = "1";
				this.menuBox.style.zIndex = "-1";
			}, ANIMATION_DURATION + ANIMATION_DELAY);
		}
	},
  handleMonthly: function() {

  },
  render: function() {
		this.menuBox = this.props.menuBox;
		this.articleBox = this.props.articleBox;
		this._handleRss = this.props.handleRss;
    var rssNodes = function (rss) {
      var favicon = "https://www.google.com/s2/favicons?domain=" + rss.rss;
			var id = "rss" + rss.id;
      return (
        <div key={rss.id} className="Rss flexbox" ref={id}>
          <img src={favicon} className="Favicon"/>
          <span className="blogTitle" onClick={this.handleRss.bind(null, rss)}>{rss.title}</span>
          <input type="checkbox" checked={rss.checked} onChange={this.onChangeCheckbox.bind(null, rss)}/>
        </div>
      );
    };
    return (
      <div className="rssList"
        onTouchStart={this.onTouchStart}
        onTouchMove={this.onTouchMove}
        onTouchEnd={this.onTouchEnd}
      >
        <div className="Rss Monthly" ref="Monthly"><span className="glyphicon glyphicon-king"></span>
        <span className="monthly" onClick={this.handleRss.bind(null, "monthly")}>{MONTHLY}</span></div>
        <br/>
        <div className="Rss RssAll" ref="RssAll"><span className="glyphicon glyphicon-align-left"></span>
          <span className="all" onClick={this.handleRss}>{ALL}</span></div>
        {this.props.rsses.map(rssNodes, this)}
      </div>
    );
  }
});

// Delete specific blog title
var DeleteButton = React.createClass({
  onDeleteButtonClick: function(title) {
    this.props.onDeleteButtonClick(title);
  },
  render: function() {
    var deleteButton = null;
    if(this.props.article.showDeleteButton) {
      deleteButton = <div className="articleDeleteButton"
        onClick={this.onDeleteButtonClick.bind(null, this.props.article.blogtitle)}>
        <span className="glyphicon glyphicon-trash"></span></div>
    }
    return (
      <div className="articleDeleteButton">{deleteButton}</div>
    );
  }
});

var Article = React.createClass({
  onTouchEnd: function(e) {
		if (getDevice() != "sp") return;
    if (this.articleRow.style.marginLeft != "") {
      if (this.marginLeft <= -100) {
        $(this.articleRow).animate(
          {"margin-left": -100, "margin-right": 100},
          {duration: ANIMATION_DURATION, queue: false}
        );
        $(this.touchButton).animate(
          {"right": -100},
          {duration: ANIMATION_DURATION, queue: false}
        );
        this.touchButton.style.zIndex = 1;
      } else if (this.marginLeft > -100) {
        $(this.articleRow).animate(
          {"margin-left": 0, "margin-right": 0},
          {duration: ANIMATION_DURATION, queue: false}
        );
        $(this.touchButton).animate(
          {"right": 0},
          {duration: ANIMATION_DURATION, queue: false}
        );
        this.touchButton.style.zIndex = -1;
      }
    }
  },
  onTouchMove: function(e) {
		if (getDevice() != "sp") return;
    if (!scrolling) {
      if ($(this.articleRow).css('margin-left') == "")
        this.marginLeft = this.articleRow.getBoundingClientRect().left;
      var x = e.nativeEvent.touches[0].clientX;
      var diffX = this.sX - x;
      if (diffX > 0) {
        this.marginLeft = this.marginLeft - Math.abs(SWIPE_SPEED*diffX/2);
      } else {
        this.marginLeft = this.marginLeft + Math.abs(SWIPE_SPEED*diffX/2);
      }
      if (this.marginLeft > -121 && this.marginLeft <= 0 - MENU_START_SCROLL_THREDHOLD/4) {
        this.touchButton.style.zIndex = -1;
        this.articleRow.style.marginLeft = this.marginLeft + "px";
        this.articleRow.style.marginRight = -this.marginLeft + "px";
        this.touchButton.style.right = this.marginLeft + "px";
      }
      this.sX = x;
    }
  },
  onTouchStart: function(e) {
		if (getDevice() != "sp") return;
    this.$window = $(window);
    this.sX = e.nativeEvent.touches[0].clientX;
    this.articleRow = this.refs.articleRow;
    this.touchButton = this.refs.touchButton;
    this.marginLeft = this.articleRow.getBoundingClientRect().left;
  },
  render: function(){
    var article_date = this.props.article.article_date.substring(5,16);
    var deleteButton;
    if (getDevice() != 'sp') {
      deleteButton = <DeleteButton article={this.props.article} onDeleteButtonClick={this.props.onDeleteButtonClick} />;
    } else {
      deleteButton = <div ref="touchButton"
        onClick={this.props.onDeleteButtonClick.bind(null, this.props.article.blogtitle)}
        className="articleDeleteTouchButton">
        <div><span className="glyphicon glyphicon-trash"></span></div>
        </div>;
    }
    return (
      <div ref="articleRow"
        className="flexBox articleRow"
        onMouseOver={this.props.displayDeleteButton.bind(null, this.props.article.id)}
        onMouseLeave={this.props.hiddenDeleteButton.bind(null, this.props.article.id)}
        onTouchStart={this.onTouchStart}
        onTouchMove={this.onTouchMove}
        onTouchEnd={this.onTouchEnd}
      >
        <div className="articleDate">{article_date}</div>
        <div className="articleTitle"><a href={this.props.article.url} target="_blank"
          onMouseDown={this.props.handleLinkClick.bind(null, this.props.article.url)}>{this.props.article.title}</a></div>
        <div className="articleBlog"><a href={this.props.article.blogurl} target="_blank">{this.props.article.blogtitle}</a></div>
        {deleteButton}
      </div>
    );
  }
});

// Article List
var ArticleList = React.createClass({
  onTouchEnd: function(e) {
		if (getDevice() != "sp") return;
    if (!this.isDelete) {
      window.clearTimeout(timeoutID);
      if (this.marginLeft <= -PAGE_WIDTH + MENU_SCROLL_THREDHOLD) {
        $(this.menuBox).animate({left: 0}, ANIMATION_DURATION);
        this.articleBox.style.zIndex = "1";
        shadow(this.menuBox, false);
        // this.menuBox.style.zIndex = -1;
      } else {
        if (this.move > 0) {
          $(this.menuBox).animate({left: 0}, ANIMATION_DURATION);
          this.articleBox.style.zIndex = "1";
          shadow(this.menuBox, false);
          // this.menuBox.style.zIndex = -1;
        } else {
          isMenu = true;
          // $(".MenuBackground").css("display", "block");
          $(this.menuBox).animate(
            {left: PAGE_WIDTH},
            {duration: ANIMATION_DURATION}
          );
        }
      }
    }
  },
  onTouchMove: function(e) {
		if (getDevice() != "sp") return;
    if (!this.isDelete && !isAnimated && !scrolling) {
      shadow(this.menuBox, true);
      var x = e.nativeEvent.touches[0].clientX;
      var diffX = this.sX - x;
      var div = this.marginLeft < 0 - MENU_START_SCROLL_THREDHOLD ? 1 : DIVISOR;
      if (diffX < 0) {
        this.marginLeft = this.marginLeft + Math.abs(SWIPE_SPEED*diffX/div);
      } else {
        this.marginLeft = this.marginLeft - Math.abs(SWIPE_SPEED*diffX/div);
      }
      if (this.marginLeft <= 0 && this.marginLeft >= -PAGE_WIDTH + MENU_START_SCROLL_THREDHOLD) {
        this.articleBox.style.zIndex = -1;
        this.menuBox.style.zIndex = 1;
        // $(this.menuBox).animate({left: this.marginLeft + PAGE_WIDTH}, {duration: MOVE_ANIMATION_DURATION, queue: false});
        this.menuBox.style.left = (this.marginLeft + PAGE_WIDTH) + "px";
      }
      this.move = this.sX - x;
      this.sX = x;
      isAnimated = true;
      timeoutID = window.setTimeout(function(){isAnimated=false;}, frameTime);
    }
    this.scrollTop = $(window).scrollTop();
  },
  onTouchStart: function(e) {
		if (getDevice() != "sp") return;
    var isDelete = false;
    var rssRows = this.props.articleBox.children[0].children;
    [].forEach.call(rssRows, function(rssRow) {
      if ($(rssRow).css('margin-left') != "0px") {
        var y = e.nativeEvent.touches[0].clientY;
        if(rssRow.getBoundingClientRect().top < y && rssRow.getBoundingClientRect().bottom > y)
        isDelete = true;
      }
    });
    this.isDelete = isDelete;
    if (!this.isDelete) {
      this.sX = e.nativeEvent.touches[0].clientX;
      this.articleBox = this.props.articleBox;
      this.menuBox = this.props.menuBox;
      this.pageList = this.props.pageList;
      this.Footer = this.props.footer;
      this.rssList = this.menuBox.children[0];
      this.header = this.props.header;
      if ($(this.rssList).height() < $(window).height()) {
        this.menuBox.style.height = $(window).height() + "px";
        this.rssList.style.height = $(window).height() + "px";
      }

      var headerHeight = $(this.header).height();
      this.scrollTop = $(window).scrollTop();
      if (this.scrollTop > headerHeight) {
        this.menuBox.style.marginTop = this.scrollTop - headerHeight + "px";
        menuTop = this.scrollTop - headerHeight;
      } else {
        this.menuBox.style.marginTop = 0;
        menuTop = 0;
      }
      menuBottom = menuTop + $(this.menuBox).height();
      this.marginLeft = this.menuBox.getBoundingClientRect().left;
      isAnimated = true;
      timeoutID = window.setTimeout(function(){isAnimated=false;}, frameTime);
    }
  },
  displayDeleteButton: function(key) {
    this.props.displayDeleteButton(key);
  },
  hiddenDeleteButton: function(key) {
    this.props.hiddenDeleteButton(key);
  },
  onDeleteButtonClick: function(title) {
    this.props.onDeleteButtonClick(title);
  },
  handleLinkClick: function(url) {
    this.props.handleLinkClick(url)
  },
  render: function() {
    var article = function (article) {
      return (
        <Article key={article.id} article={article} displayDeleteButton={this.displayDeleteButton}
          hiddenDeleteButton={this.hiddenDeleteButton} onDeleteButtonClick={this.onDeleteButtonClick}
          handleLinkClick={this.handleLinkClick} />
      );
    };
    return (
      <div className="articleList"
        onTouchStart={this.onTouchStart}
        onTouchMove={this.onTouchMove}
        onTouchEnd={this.onTouchEnd}
      >
        {this.props.articles.map(article, this)}
      </div>
    );
  }
});

// Pagination
var PagesList = React.createClass({
  handlePage: function(page, e) {
    if(e.nativeEvent.which != 2) {
      e.nativeEvent.preventDefault();
      this.props.handlePage(page);
    }
  },
  render: function() {
    var pageNodes = function(page){
      if (page.at) {
        if (page.page == "") return null;
        return (<div className="page" key={page.page}>{page.page}</div>);
      } else {
				var blogid = getBlogId();
        var _page;
        var url = blogid ? "blog/" + blogid : "";
        if(page.page == "<") {
          _page = (Number(getPage()) - 1);
          if (page.url) url = "?p=" + _page;
        } else if(page.page == ">") {
          _page = (Number(getPage()) + 1);
          if (page.url) url = "?p=" + _page;
        } else {
          _page = page.page;
          url = "?p=" + page.page;
        }
        return (
          <div className="page" key={page.page}>
            <a href={url} onClick={(e) => this.handlePage(_page, e)}>
            {page.page}
            </a>
          </div>
        );
      }
    };
    return (
      <div className="flexBox pageList">
        {this.props.pages.map(pageNodes, this)}
      </div>
    );
  }
});

// App
var AntennaApp = React.createClass({
  // Initial state for Articles
  getInitialState: function() {
		console.log(getDevice());
    return {
      articles: [],
      pages: [],
      rsses: [],
      at: getPage(),
      page_limit: getDevice() == 'sp' ? MOBILE_PAGE_LIMIT : PAGE_LIMIT
    }
  },

  // Limit too much re-rendering
  // shouldComponentUpdate: function(nextProps, nextState) {
  //   return this.state.articles !== nextState.articles;
  // },

  // After dom loaded
  componentDidMount: function() {
    headerHeight = $(this.refs.Header).height();
		if (getBlogId()) {
			this.loadRssesFromServer(this.loadArticlesFromServer, this.loadPageNumFromServer);
		} else if (isRanking()) {
      this.setState({pages: []});
      this.loadRssesFromServer();
		  this.loadMonthlyFromServer();
		} else {
			this.loadRssesFromServer();
			this.loadArticlesFromServer();
			this.loadPageNumFromServer();
		}
    window.addEventListener('popstate', this.handleBackPage, {passive: true});
    window.addEventListener('scroll', this.handleScroll, {passive: false});
  },

  // After component unmount
  componentWillUnmount: function() {
    window.removeEventListener('popstate', this.handleBackPage);
    window.removeEventListener('scroll', this.handleScroll);
  },

  // Set url page numbers
  handlePage: function(page) {
		var blogid = getBlogId();
    this.setState({at:page});

		// changing url
		var url = "/";
		url = blogid ? url + "blog/" + blogid +"/" : url;
    url = page == 1 ? url : url + "?p=" + page;
    window.history.pushState(null,null,url);

		// get single Blog rsses if blog id exists
		var rsses = blogid ? this.getBlogRsses() : [];

		// loading articles and pages;
		this.loadArticlesFromServer(page, rsses);
		this.loadPageNumFromServer(rsses);

		// scroll to top if device is smartphone
    if (getDevice() == "sp") document.body.scrollTop = document.documentElement.scrollTop = 0;
  },

	// handling when rss clicked
	handleRss: function(rss) {
		var url = "/"
		var _rsses = copy(this.state.rsses);
		if (rss && rss != "monthly") {
			$.each(_rsses, function(index, _rss) {
				_rss.id == rss.id ? _rss.checked = true : _rss.checked = false;
			});
			// making url;
			url = url + "blog/" + rss.id + "/";
		} else if (rss && rss == "monthly") {
		  url = url + "monthly";
		}
		window.history.pushState(null,null,url);

    if (rss == "monthly") {
      this.setState({pages: []});
      this.loadMonthlyFromServer();
    } else {
  		this.loadArticlesFromServer(1, _rsses);
  		this.loadPageNumFromServer(_rsses);
    }
	},

  // Function with browser back button pushed
  handleBackPage: function() {
    if (isRanking()) {
      // console.log("ranking")
      this.loadMonthlyFromServer();
      return;
    }
		var blogid = getBlogId();
    var page = getPage();
		if (!blogid) {
	    if (page) {
	      this.setState({at:page});
	      this.loadArticlesFromServer(page);
	      this.loadPageNumFromServer();
	    }
		} else {
			var _rsses = copy(this.state.rsses);
			$.each(_rsses, function(index, _rss) {
				_rss.id == blogid ? _rss.checked = true : _rss.checked = false;
			});
			if (page) {
				this.setState({at:page});
				this.loadArticlesFromServer(page, _rsses);
				this.loadPageNumFromServer(_rsses);
			} else {
				this.loadArticlesFromServer(1, _rsses);
				this.loadPageNumFromServer(_rsses);
			}
		}
  },

  // Displaying delete button when hovered
  displayDeleteButton: function(key) {
    var _articles = [];
    $.each(this.state.articles, function(i, article) {
      if(article.id == key) article.showDeleteButton = 1;
      _articles.push(article);
    });
    this.setState({articles: _articles});
  },

  // Hidden delete button when left from row
  hiddenDeleteButton: function(key) {
    var _articles = [];
    $.each(this.state.articles, function(i, article) {
      if(article.id == key) article.showDeleteButton = 0;
      _articles.push(article);
    });
    this.setState({articles: _articles});
  },

  // When delete button clicked
  onDeleteButtonClick: function(title) {
    if(window.confirm(CONFIRM_DIALOG)) {
      this.handleDelete(title);
    }
  },

  handleDelete: function(title) {
    var checkboxes = reactCookie.load("checkboxes");
    if(checkboxes == undefined) checkboxes = [];
    var exists = false;
    checkboxes.forEach(function(checkbox) {
      if (checkboxes.title == title) exists = true;
    });
    if(!exists) {
      var rsses = this.state.rsses;
      var checkbox;
      rsses.forEach(function(rss) {
        if (rss.title == title) {
          rss.checked = false;
          checkbox = rss;
        }
      });
      this.setState({rsses: rsses});
      checkboxes.push(checkbox);
      reactCookie.save("checkboxes", checkboxes);
      this.loadArticlesFromServer(getPage());
      this.loadPageNumFromServer();
    }
  },

  onChangeCheckbox: function (rss) {
		// reverse checked
    rss.checked = !rss.checked;
    var rsses = this.state.rsses;
    rsses.forEach(function(_rss) {
      if (_rss.rss == rss.rss) _rss = rss;
    });

		// loading checkboxes from cookie;
    var checkboxes = reactCookie.load("checkboxes");
    if (checkboxes) {
      var exists = false;
      for (var i = 0; i < checkboxes.length; i++) {
        if (checkboxes[i].rss == rss.rss) { checkboxes[i] = rss; exists = true; }
      }
      if (!exists) checkboxes.push(rss);
    } else {
      checkboxes = [rss];
    }
    reactCookie.save("checkboxes", checkboxes);

		// changing url (reset to root)
		window.history.pushState(null, null, "/");

		// load articles first using this checkboxes setting
    this.loadArticlesFromServer(1, checkboxes);
    this.loadPageNumFromServer(checkboxes);
    this.setState({rsses: rsses});
		this.setState({at: 1});
  },

	calcPageNum: function (at, page_limit, _lastPage) {
		var pages = [];
		var firstPage = at - Math.ceil(page_limit * 5 / 10);
		var lastPage = at + Math.floor(page_limit * 4 / 10);
		if (firstPage <= 1) {
			var tmp = firstPage;
			firstPage = 1;
			lastPage = lastPage + (1 - tmp);
		}
		if (at <= 1) {
			pages.push({page: "", at: 1, url: false});
		} else {
			pages.push({page: "<", at: 0, url: true});
		}
		if (lastPage > _lastPage.num) lastPage = _lastPage.num;
		for(var page = firstPage; page <= lastPage; page++) {
			pages.push({page: page, at: page == at ? 1 : 0, url: true});
		}
		if (lastPage == at) {
			pages.push({page: "", at: 1, url: false});
		} else {
			pages.push({ page: ">", at: 0, url: true});
		}
		return pages;
	},

	// getting rsses with one rss checked which you had clicked;
	getBlogRsses: function () {
		var blogid = getBlogId();
		var rsses = copy(this.state.rsses);
		if (blogid) {
			$.each(rsses, function(index, rss) {
				rss.checked = rss.id == blogid ? true : false;
			});
		}
		return rsses;
	},

  // trigger when article link clicked used for views
  handleLinkClick: function(url, e) {
    $.ajax({
      url: "/../api/addView",
      dataType: 'json',
      data: {"url": url},
      cache: false,
      type: 'POST',
      success: function(views) {
        // console.log(views);
      }.bind(this),
      error: function(xhr, status, err) {
        console.error("dummy", status, err.toString());
      }.bind(this)
    });
  },

  handleScroll: function(e) {
    if (!this.state.moreLoading && !isRanking()) {
      if ($(window).scrollTop() + $(window).height() > ($(document).height() * MORE_LOAD_THREDHOLD)) {
        this.setState({moreLoading: true});
        this.loadMoreArticlesFromServer();
      }
    }
  },

	mixCheckboxes: function(checkboxes, rsses) {
		if (checkboxes != undefined) {
			checkboxes.forEach(function(checkbox) {
				rsses.forEach(function(rss) {
					if (rss.rss == checkbox.rss) {
						rss.checked = checkbox.checked;
						return;
					} else {
						if(rss.checked != false) rss.checked = true;
					}
				});
			});
		}
		return rsses;
	},

  loadMoreArticlesFromServer: function() {
		this.loading = this.refs.loading;
		$(this.loading).addClass("loading");
    var page = this.state.at + 1;
		var rsses = this.mixCheckboxes(reactCookie.load("checkboxes"), this.state.rsses);
		if (getBlogId()) {
			rsses = this.getBlogRsses();
		}
		var data = {
			page: page,
			checkboxes: rsses
		};

    this.setState({at: page});

    $.ajax({
      url: "/../api/listArticles",
      dataType: 'json',
      data: data,
      cache: false,
      type: 'POST',
      success: function(articles) {
        var _articles = this.state.articles;
        articles.forEach((article) => { article.showDeleteButton = 0; _articles.push(article); });
        this.setState({articles: _articles});
        this.setState({moreLoading: false});
				$(this.loading).removeClass("loading");
      }.bind(this),
      error: function(xhr, status, err) {
        console.error("dummy", status, err.toString());
      }.bind(this)
    });
  },

  // Load Articles from the Server
  loadArticlesFromServer: function(page, checkboxes) {
		// getting all rsses with one rss checked which you clicked;
		var rsses = this.getBlogRsses();
		var data = {};

		// if none page assigned, use clicked rss one or from cookie
		if (!page) {
			if (rsses.length == 0) rsses = reactCookie.load("checkboxes");
			page = this.state.at;
      data = {
	      page: page,
	      checkboxes: rsses
			};
    } else {
			data = {
				page: page,
				checkboxes: checkboxes
			};
		}

    $.ajax({
      url: "/../api/listArticles",
      dataType: 'json',
      data: data,
      cache: false,
      type: 'POST',
      success: function(articles) {
        var _articles = [];
        articles.forEach((article) => { article.showDeleteButton = 0; _articles.push(article); });
        this.setState({articles: _articles});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error("dummy", status, err.toString());
      }.bind(this)
    });
  },

  // Load pagination numbers
  loadPageNumFromServer: function(checkboxes) {
    var data = {
      checkboxes: checkboxes ? checkboxes : reactCookie.load("checkboxes")
    };

    $.ajax({
      url: "/../api/getPageNum",
      dataType: 'json',
      data: data,
      cache: false,
      type: 'POST',
      success: function(_lastPage) {
        var pages = [];
        var at = Number(this.state.at);
				var page_limit = this.state.page_limit;
        this.setState({pages: this.calcPageNum(at, page_limit, _lastPage)});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error("dummy", status, err.toString());
      }.bind(this)
    });
  },

  // Load RSSes from the Server
  loadRssesFromServer: function(loadArticle, loadPageNum) {
    $.ajax({
      url: "/../api/listRsses",
      dataType: 'json',
      cache: false,
      success: function(rsses) {
				if (!getBlogId()) {
	        var checkboxes = reactCookie.load("checkboxes");
	        if (checkboxes) {
	          rsses = this.mixCheckboxes(checkboxes, rsses);
	        } else {
	          rsses.forEach(function(rss) { rss.checked = true; });
	        }
				} else {
					rsses.forEach(function(rss) { rss.checked = true; });
				}
				loadArticle ? this.setState({rsses: rsses}, loadArticle) : this.setState({rsses: rsses});
				if (loadPageNum) loadPageNum(this.getBlogRsses());
      }.bind(this),
      error: function(xhr, status, err) {
        console.error("dummy", status, err.toString());
      }.bind(this)
    });
  },

  loadMonthlyFromServer: function () {
    $.ajax({
      url: "/../api/monthlyRanking",
      dataType: 'json',
      cache: false,
      success: function(articles) {
        this.setState({articles: articles});
      }.bind(this),
      error: function(xhr, status, err) {
        console.error("dummy", status, err.toString());
      }.bind(this)
    });
  },

  // Whole html for List Article App
  render: function() {
    return (
      <div className="Body" onTouchMove={this.onTouchMove}>
				<div ref="loading"></div>
        <div className="Header" ref="Header">
					<Ads />
          <div className="HeaderContents">
            <h1><a href="/">{ANTENNA_TITLE}</a></h1>
            <span className="subtitle">{ANTENNA_SUBTITLE}</span>
						<div className="HeaderMenu flexBox">
							<a href="/updates">updates</a>&nbsp;<a href="/about">about</a>&nbsp;<span className="Rights">{COPYRIGHT}</span>
						</div>
          </div>
        </div>
        <div className="Contents flexBox">
          <div className="MenuBox" ref="MenuBox">
            <RssList
              rsses={this.state.rsses}
              onChangeCheckbox={this.onChangeCheckbox}
							handleRss={this.handleRss}
              menuBox={this.refs.MenuBox}
              articleBox={this.refs.ArticleBox}
            />
          </div>
          <div className="ArticleBox" ref="ArticleBox">
            <ArticleList
              articles={this.state.articles}
              displayDeleteButton={this.displayDeleteButton}
              hiddenDeleteButton={this.hiddenDeleteButton}
              onDeleteButtonClick={this.onDeleteButtonClick}
              handleLinkClick={this.handleLinkClick}
              menuBox={this.refs.MenuBox}
              articleBox={this.refs.ArticleBox}
              header={this.refs.Header}
            />
            <PagesList ref="PageList" pages={this.state.pages} handlePage={this.handlePage} />
          </div>
        </div>
      </div>
    );
  }
});

// Init
renderApp();
