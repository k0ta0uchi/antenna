const DATA_AD_CLIENT = "ca-pub-1522638092941955";
const DATA_AD_SLOT = "1871027029";
const ANTENNA_TITLE = "白猫アンテナ";
const ANTENNA_SUBTITLE = "スマホゲーム、「白猫プロジェクト」の攻略まとめアンテナ";
const COPYRIGHT = "© 2016 白猫アンテナ";
const CONFIRM_DIALOG = "選択したブログを非表示にしますか？元に戻したい場合はサイドバーのチェックボックスにチェックを入れてください。";
const MONTHLY =  "月間ランキング";
const ALL = "All";
