<?php
// Get new container
$container = $app->getContainer();

// Logger
$container['logger'] = function($c) {
    $logger = new \Monolog\Logger('my_logger');
    $file_handler = new \Monolog\Handler\StreamHandler("../logs/app.log");
    $logger->pushHandler($file_handler);
    return $logger;
};

// Service factory for the ORM
$container['db'] = function ($c) {
    $capsule = new \Illuminate\Database\Capsule\Manager;
    // $container['logger']->debug($c->get('settings')['db']['database']);

    $capsule->addConnection($c->get('settings')['db']);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();

    return $capsule;
};

// Register PHP-Renderer
$container['view'] = function ($container) {
    return new \Slim\Views\PhpRenderer('templates/');
};
