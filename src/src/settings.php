<?php
return [
    'settings' => [
        'displayErrorDetails' => true,
        'addContentLengthHeader' => false,
        'db' => [
            'driver' => 'sqlite',
            'database' => __DIR__ . '/../db/antenna.sqlite',
            'prefix' => '',
            // 'host' => 'localhost',
            // 'username' => 'root',
            // 'password' => '898WBUS2',
            // 'charset' => 'utf8',
            // 'collation' => 'utf8_unicode_ci',
        ],
    ],
];
