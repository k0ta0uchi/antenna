<?php
require 'vendor/autoload.php';

// Set up settings
$settings = require __DIR__ . '/src/settings.php';
$app = new \Slim\App($settings);
$app->add(new \pavlakis\cli\CliRequest());

// Set up dependencies
require __DIR__ . '/src/dependencies.php';

$app->getContainer()->get('db');

$app->get('/cron', function() {
    $rsses = Rss::all();

    foreach ($rsses as $rss) {
        $regexp = "/\.notrss/";
        if (preg_match($regexp, $rss->rss)) {
            continue;
        }
        try {
            $rssfeed = Feed::loadRss($rss->rss);
            if (!count($rssfeed->item)) {
                $rssfeed = simplexml_load_file ( $rss->rss );
                if ($rssfeed === false) {
                    throw new FeedException();
                }
            }
        } catch (FeedException $e) {
            $rssfeed = Feed::loadAtom($rss->rss);
        } catch (FeedException $e) {
            $this->logger->addError("failed loading feed: " . $rss->rss);
            continue;
        }

        if ($rssfeed->entry) {
            $items = $rssfeed->entry;
        } else {
            $items = $rssfeed->item;
        }

        foreach ($items as $item) {
            if ($item->link['href']) {
                if($item->link['type'] == 'application/atom+xml') {
                    $url = $item->link[2]['href'];
                } else {
                    $url = $item->link['href'];
                }
            } else {
                $url = $item->link;
            }
            $dc = $item->children('http://purl.org/dc/elements/1.1/');
            if (!(Article::where('url', $url)->count())) {
                $article = new Article();
                $article->rss = $rss->rss;
                $article->url = $url;
                $article->title = $item->title;
                if ($rssfeed->channel->link) {
                    $article->blogurl = $rssfeed->channel->link;
                    $article->blogtitle = $rssfeed->channel->title;
                } else if ($rssfeed->link != ""){
                    $article->blogurl = $rssfeed->link;
                    $article->blogtitle = $rssfeed->title;
                } else {
                    foreach($rssfeed->link->attributes()['href'] as $a => $b) {
                        $article->blogurl = $b;
                    }
                    $article->blogtitle = $rssfeed->title;
                }
                if ($article->blogurl == "") {
                    $article->blogurl = $rssfeed->link[0]['href'];
                }
                $article->views = 0;
                if ($dc->date) {
                    $article->article_date = date("Y-m-d H:i:s", strtotime($dc->date));
                } elseif ($item->published) {
                    $article->article_date = date("Y-m-d H:i:s", strtotime($item->published));
                } elseif ($item->pubDate) {
                    $article->article_date = date("Y-m-d H:i:s", strtotime($item->pubDate));
                }
                $article->save();
            }
        }
    }
});

$app->get('/cronnonrss', function() {
    $targets = require __DIR__ . '/src/scrapeurls.php';

    $client = new \Goutte\Client();

    foreach($targets as $target) {
        if (!(Rss::where('rss', $target["url"] . ".notrss")->count())) {
            $rssrow = new Rss();
            $rssrow->rss = $target["url"] . ".notrss";
            $rssrow->url = $target["url"];
            $rssrow->title = $target["title"];
            $rssrow->save();
        }

        $c = $client->request('GET', $target["url"]);
        $urls = $c->filter($target['articleurl']['tag'])->each(function ($node, $i) use ($target) {
            if (isset($target['articleurl']['attr'])) {
                $url = $node->attr($target['articleurl']['attr']);
            } else {
                $url = $node->text();
            }
            return isset($target['articleurl']['opt']) ? $target['articleurl']['opt'] . $url : $url;
        });

        $titles = $c->filter($target['articletitle']['tag'])->each(function ($node, $i) use ($target) {
            if (isset($target['articletitle']['attr'])) {
                $title = $node->attr($target['articletitle']['attr']);
            } else {
                $title = $node->text();
            }
            return $title;
        });

        $dates = $c->filter($target['date'])->each(function ($node, $i) {
            return $node->text();
        });

        for ($i = 0; $i < count($urls); $i++) {
            if (!(Article::where('url', $urls[$i])->count())) {
                $article = new Article();
                $article->rss = $target["url"] . ".notrss";
                $article->url = $urls[$i];
                $article->title = $titles[$i];
                $article->blogurl = $target["url"];
                $article->blogtitle = $target["title"];
                $article->views = 0;
                $date = $dates[$i];
                if ($target["regexp"]["regexp"] != "") {
                    $regexp = $target["regexp"]["regexp"];
                    preg_match($regexp, $dates[$i], $matches);
                    $date = "";
                    for ($j = 1; $j <= $target["regexp"]["matchnum"]; $j++) {
                        if ($j == $target["regexp"]["matchnum"]) {
                            $date = $date . $matches[$j];
                        } else {
                            $date = $date . $matches[$j] . $target["regexp"]["between"];
                        }
                    }
                }
                $_date = date("Y-m-d H:i:s", strtotime($date));
                if ($_date == "1970-01-01 09:00:00") {
                  if (isset($target["dateformat"])) {
                    if ($target["dateformat"]["format"] == "before") {
                      preg_match("/.*?([0-9]*)?.*/", $date, $matches);
                      $now = new DateTime();
                      echo var_dump($matches);
                      echo $now->modify('-'.$matches[0].' '.$target["dateformat"]["unit"]);
                    }
                  }
                }
                $article->article_date = date("Y-m-d H:i:s", strtotime($date));
                $article->save();
            }
        }
    }
});

$app->run();
